var _ = require("lodash");
var shuffle = require("shuffle-array");

/*  create array with 13 card ranks
    create array with 4 suits (Hearts, Tiles, Clubs, Spades)
    create deck with 52 cards */
const createDeck = () => {

  const ranks = _.range(2,15);
  const suits = ['H','T','C','S'];
  const deck = [];

  suits.forEach( suit => {
    ranks.forEach( rank => {
      deck.push({
        "suit" : suit,
        "rank" : rank
      })
    });
  });

  return deck;
}

const checkHand = hand => {

  /* multOfRanks is an array that saves the multiplicity
   of each rank going from lowest to highest rank */
  const multOfRanks = new Array(13).fill(0);

  let flush = true;
  const flushSuit = hand[0].suit;

  hand.forEach( obj => {
    multOfRanks[obj.rank - 2]++;
    if (flushSuit!==obj.suit) flush = false;
  });

  const ace = multOfRanks[12];
  let straight = false;
  let pairs = 0;
  let triplets = 0;
  let quadrants = 0;
  let counter = 0;

  for (const mult of multOfRanks){
    switch (mult) {
      case 4: quadrants++; break;
      case 3: triplets++; break;
      case 2: pairs++; break;
      case 1: counter++; if(counter===5) straight = true; break;
      case 0: counter = 0;
    }
  }

  if (straight===true){
    if (flush===true){
      if (ace>0){
        console.log("Royal Flush!!");
      }else{
        console.log("Straight Flush");
      }
    }else{
      console.log("Straight");
    }
  }else{
    if (flush===true){
      console.log("Flush");
    }else{
      if (quadrants===1){
        console.log("Four of a Kind");
      }else if (triplets===1){
        if(pairs===1){
          console.log("Full House");
        }else{
          console.log("Three of a Kind");
        }
      }else if (pairs===2){
        console.log("Two Pairs");
      }else if (pairs===1){
        console.log("One Pair");
      }else{
        console.log("No Pair :(");
      }
    }
  }

}

const deck = createDeck();
const hand = shuffle.pick( deck, { 'picks': 5 });
console.log(hand);
checkHand(hand);
